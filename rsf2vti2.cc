/*
 *  Export VTI (Visualization Toolkit Images ) files for RSF data.
 *  The VTI file created can be loaded into ParaView using the 
 *  File -> Open menu option.  
 *  Copyright (C) 2019-2021-2022.
 *  by Herling Gonzalez-Alvarez,	gherling@gmail.com 
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include<valarray>
#include<cstdio>
#include<cstdlib>

using namespace std;
extern "C" {
  #include<rsf.h>
  #include<omp.h>
}

#include <vtkXMLImageDataWriter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
  
#define J(i,j,k)  (i)*ny*nz + (j)*nz + (k) 

void write_vtk_data (char *fileName, int mode, const sf_file& rsfFile)
{
  int nz,ny,nx;
  float dz,dy,dx;
  float o1,o2,o3;

  //Read volumen axes 
  sf_axis axis_z = sf_iaxa(rsfFile,1); //first axis
  nz = sf_n(axis_z);      //number dimension
  dz = sf_d(axis_z);      //grid size
  o1 = sf_o(axis_z);      //origin
   
  sf_axis axis_y = sf_iaxa(rsfFile,2); //second axis
  ny = sf_n(axis_y);      //number dimension
  dy = sf_d(axis_y);      //grid size 
  o2 = sf_o(axis_y);      //origin
   
  sf_axis axis_x = sf_iaxa(rsfFile,3); //third axis 
  nx = sf_n(axis_x);      //number dimension
  dx = sf_d(axis_x);      //grid size
  o3 = sf_o(axis_x);      //origin


  vtkSmartPointer<vtkImageData> 
  imageData = vtkSmartPointer<vtkImageData>::New();

  imageData->SetDimensions(nx,ny,nz);
  imageData->SetSpacing(dx,dy,dz);
//imageData->SetOrigin(o3,o2,nz*dz-o1);
  imageData->SetOrigin(o3,o2,o1);

  switch(mode) {
    case 0 : { 
      std::valarray<unsigned char> data(nx*ny*nz); 
      sf_ucharread(&data[0],data.size(),rsfFile);//read volumen
      imageData->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
      unsigned char* voxel = static_cast<unsigned char*>(imageData->GetScalarPointer());
      for (int i=0; i<data.size(); i++) {
        voxel[i] = data[i];
      }
      break;
    }
    case 1: {
      std::valarray<float> data(nx*ny*nz); 
      sf_floatread(&data[0],data.size(),rsfFile);//read volumen
      imageData->AllocateScalars(VTK_FLOAT, 1);
      float* voxel = static_cast<float*>(imageData->GetScalarPointer());
      for (int i=0; i<data.size(); i++) {
        voxel[i] = data[i];
      }
      break; //declares volumen data
    }
    default :
      exit(1);
  }

  vtkSmartPointer<vtkXMLImageDataWriter> 
  writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
  writer->SetFileName(fileName);
  writer->SetInputData(imageData);
  writer->Write();
}

int main(int argc, char* argv[]) {
  
  char *nameFile=NULL;
  int mode=0;

  if(argc<2){
    fprintf(stdout,"\n%s < file.rsf file=file.vti mode=[0=unsigned char, 1=float]\n",argv[0]);
    exit(1);
  }

  // initialize RSF-API  
  sf_init(argc,argv);

  // setup I/O files
  sf_file RSFin = sf_input("in"); //volume input

  if( NULL==(nameFile=sf_getstring("file")) )
    sf_error("Need file=");//Specify VTK file extension (*.vti)
  
  // parameter from the command line 
  // outputs char values (1 byte) or float (4 byte) values
  if (!sf_getint("mode",&mode)) 
      sf_error("Need mode=[0=unsigned char, 1=float]" );

  write_vtk_data(nameFile,mode,RSFin);

  sf_close();
}
